FROM openjdk:8-jre

COPY target/vault-chat.jar /opt/vault-chat/

WORKDIR /opt/vault-chat

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-jar", "./vault-chat.jar"]
