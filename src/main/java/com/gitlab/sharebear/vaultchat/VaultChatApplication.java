package com.gitlab.sharebear.vaultchat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VaultChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaultChatApplication.class, args);
	}

	@Bean
	Exchange chatExchange() {
		return ExchangeBuilder
				.fanoutExchange("chat.exchange")
				.durable(true)
				.build();
	}

	@Bean
	Queue chatQueue() {
		return QueueBuilder
				.durable("chat.queue")
				.build();
	}

	@Bean
	Binding chatBinding(Exchange exchange, Queue queue) {
		return BindingBuilder
				.bind(queue)
				.to(exchange)
				.with("")
				.noargs();
	}

	@Bean
	Jackson2JsonMessageConverter messageConverter(ObjectMapper objectMapper) {
		return new Jackson2JsonMessageConverter(objectMapper);
	}

}
