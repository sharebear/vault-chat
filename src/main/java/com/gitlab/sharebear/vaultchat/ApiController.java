package com.gitlab.sharebear.vaultchat;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ApiController {

    private final RabbitTemplate rabbitTemplate;
    private final Exchange exchange;
    private final MessageStore messageStore;

    @GetMapping(path = "/api/messages")
    public List<Message> getMessages() {
        log.info("foobar");
        return messageStore.getMessages();
    }

    @PostMapping(path = "/api/messages")
    public void sendMessage(@RequestBody SendMessageCommand command) {
        log.info("wooo {}", command);
        rabbitTemplate.convertAndSend(
                exchange.getName(),
                "message",
                new Message(Instant.now(), command.getMessage())
        );
    }
}
