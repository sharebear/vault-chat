package com.gitlab.sharebear.vaultchat;

import lombok.Value;

@Value
public class SendMessageCommand {
    String message;
}
