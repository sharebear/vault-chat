package com.gitlab.sharebear.vaultchat;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class MessageListener {

    private final MessageStore messageStore;

    @RabbitListener(queues = "#{chatQueue.name}")
    void handleMessage(@Payload Message message) {
        log.info("Got a damn message!!! {}", message);
        messageStore.addMessage(message);
    }
}
