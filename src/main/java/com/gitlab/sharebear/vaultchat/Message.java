package com.gitlab.sharebear.vaultchat;

import lombok.Value;

import java.time.Instant;

@Value
public class Message {
    Instant timestamp;
    String message;
}
