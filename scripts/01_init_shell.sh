#!/usr/bin/env bash
export VAULT_ADDR=http://localhost:8200

VAULT_TOKEN=$(docker-compose logs vault | grep Root | tail -n 1 | awk '{print $5}')
export VAULT_TOKEN
