path "secret/data/reactless-chat" {
    capabilities = [ "read" ]
}

path "secret/data/application" {
    capabilities = [ "read" ]
}
